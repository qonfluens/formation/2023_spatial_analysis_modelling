#!/bin/bash

DOCKER_REGISTRY="qonfluens"
IMAGE_NAME="emme"

TAG=$(git describe --exact-match --tags HEAD 2>/dev/null)
if [[ ! -z $TAG ]]; then
    echo "found tag $TAG"
    # docs.docker.com:
    # The tag must be valid ASCII and can contain lowercase and uppercase
    # letters, digits, underscores, periods, and hyphens. It cannot start with
    # a period or hyphen and must be no longer than 128 characters.
    if [[ ! "$TAG" =~ [0-9a-zA-Z_][0-9a-zA-Z._-]{0,126} ]]; then
        echo "incorrect tag syntax for docker images. Images NOT tagged"
    else
        if [[ -z $(git ls-remote origin refs/tags/$TAG) ]]; then
            echo "WARNING, tag $TAG missing in git remote !"
            echo "image NOT tagged"
        else
            if [[ -z $(git status -s) ]]; then
                echo "git status is clean."
                export TAG_NAME=$TAG
            else
                echo "WARNING, git status is not clean."
                git diff
                read -p "tag image anyway ? [y/*]" -n 1 -r
                echo
                if [[ $REPLY =~ ^[Yy]$ ]]; then
                    export TAG_NAME=$TAG
                else
                    echo "image NOT tagged"
                fi
            fi
        fi
    fi
fi


docker build -t $DOCKER_REGISTRY/$IMAGE_NAME .
echo "building $DOCKER_REGISTRY/$IMAGE_NAME..."
echo "docker login -u $DOCKER_REGISTRY"
echo "docker push $DOCKER_REGISTRY/$IMAGE_NAME"
if [ ! -z $TAG_NAME ]; then
    docker build -t $DOCKER_REGISTRY/$IMAGE_NAME:$TAG_NAME .
    echo "docker push $DOCKER_REGISTRY/$IMAGE_NAME:$TAG_NAME"
fi
