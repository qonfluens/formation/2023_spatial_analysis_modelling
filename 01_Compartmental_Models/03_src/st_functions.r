#' @title Test if an \link[sf]{sf} is a square
#' 
#' @description  Test if an sf is a square, with tolerance. Default is 5% tolerance.
#' 
#' @param .sf and object of class \link[sf]{sf}
#' @param tolerance tolerance rate between both square side length
#' 
#' @export
#' 
is_square_sf <- function(.sf, tolerance=0.05){
  bbox <- st_bbox(.sf)
  x_length <- as.numeric(abs(abs(bbox$xmax) - abs(bbox$xmin)))
  y_length <- as.numeric(abs(abs(bbox$ymax) - abs(bbox$ymin)))
  limit_min <- (x_length + y_length)/2 * (1-tolerance)
  limit_max <- (x_length + y_length)/2 * (1+tolerance)
  
  return(all(x_length <= limit_max, limit_min <= x_length,
             y_length <= limit_max, limit_min <= y_length)
  )
}


#' @title Add squared frame polygon
#' 
#' @description Return a square frame surrounding a list of sf
#' 
#' @param list_sf A list of objects of class \link[sf]{sf} 
#' @param buffer_dist numeric; buffer distance for all, or for each of the elements in x;
#'  in case dist is a units object, it should be convertible to arc_degree
#'   if x has geographic coordinates, and to st_crs(x)$units otherwise. 
#'   See function `sf_buffer` from package `sf` for details
#' 
#' @export
#' 
st_squared_geometry <- function(list_sf, buffer_dist = NULL){
  if(length(list_sf)!=1){
    if(!do.call(all.equal, lapply(list_sf, function(i) st_crs(i)$epsg))){
      stop("coordinate reference system differ between elements of list")
    }
  }
  map_bbox <- sapply(list_sf, st_bbox)
    
  map_lat_min <- min(map_bbox["ymin",])
  map_lat_max <- max(map_bbox["ymax",])
  map_lon_min <- min(map_bbox["xmin",])
  map_lon_max <- max(map_bbox["xmax",])
  
  if(map_lat_max-map_lat_min > map_lon_max-map_lon_min){
    # longest side = latitude
    map_lon_mid <- map_lon_max - (map_lon_max-map_lon_min)/2
    map_lon_min <- map_lon_mid - (map_lat_max-map_lat_min)/2
    map_lon_max <- map_lon_mid + (map_lat_max-map_lat_min)/2
  } else{
    # longest side = lonitude
    map_lat_mid <- map_lat_max - (map_lat_max-map_lat_min)/2
    map_lat_min <- map_lat_mid - (map_lon_max-map_lon_min)/2
    map_lat_max <- map_lat_mid + (map_lon_max-map_lon_min)/2
  }
  
  # For polygon, we need to close it: first and last points must be identical!!!
  df <- data.frame(lon = c(map_lon_min, map_lon_min, map_lon_max, map_lon_max, map_lon_min),
                   lat = c(map_lat_min, map_lat_max, map_lat_max, map_lat_min, map_lat_min))
  
  frame <- st_sfc(st_polygon(list(as.matrix(df))), crs = st_crs(list_sf[[1]])$proj4string)
  sf_frame <- sf::st_sf(geometry=frame)
  if(!is.null(buffer_dist)){
    sf_frame <- st_buffer(sf_frame, dist = buffer_dist)
  }
  return(sf_frame)
}