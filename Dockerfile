FROM jupyter/r-notebook

USER root
RUN sudo apt update

RUN conda install -c conda-forge -y r-desolve && \
    conda install -c conda-forge -y r-ggplot2 && \
    conda install -c conda-forge -y r-tidyr && \
    conda install -c conda-forge -y r-fftwtools && \
    conda install -c conda-forge -y r-sf && \
    conda install -c conda-forge -y r-leaflet && \
    conda install -c conda-forge -y r-fasterize && \
    conda install -c conda-forge -y r-coda && \
    conda install -c conda-forge -y r-rjags && \
    conda clean --all

RUN rm -rf work

ENV PROJ_LIB /opt/conda/pkgs/proj-9.3.0-h1d62c97_2/share/proj/
COPY init_aem_files.sh /usr/bin/
