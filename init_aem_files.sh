#!/bin/bash

DONE_FLAG=.init-done
if [ ! -f $DONE_FLAG  ]; then
    BACKUP=".backup_$(date +%F-%Hh%M)"
    mkdir -p $BACKUP
    mv ./* $BACKUP
    GITLAB_URL=https://gitlab.com/qonfluens
    AEM_URL=$GITLAB_URL/formation/2023_spatial_analysis_modelling
    ARTIFACT_URL=$AEM_URL/-/jobs/artifacts/main/download?job=aem-files
    ARCHIVE=/tmp/artifact.zip
    ARCHIVE_TOP=aem-qonfluens

    wget $ARTIFACT_URL -O $ARCHIVE && \
    unzip $ARCHIVE && \
    mv $ARCHIVE_TOP/* . && \
    rm -rf $ARCHIVE_TOP $ARCHIVE && \
    touch $DONE_FLAG
fi

