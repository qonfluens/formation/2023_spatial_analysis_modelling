# 2023_spatial_analysis_modelling

Lecture for the EMME Master 2 at the university of Franche-Comté.

## LICENSE

Copyright (c) 2023 by Qonfluens SAS. This work is licensed under the Creative
Commons BY-SA 4.0 License. To wiew a copy of this license, visit
https://creativecommons.org/licenses/by-sa/4.0/
